use ndarray::{Array2, Ix2};

use std::path::Path;

use error_chain::error_chain;

error_chain! {
    foreign_links {
        Netcdf(netcdf::error::Error);
    }

    errors {
        NonExistingVariable {
            description("Unable to find variable"),
            display("Unable to find variable"),
        }
    }
}

const RESOLUTION_X: isize = 8;
const RESOLUTION_Y: isize = RESOLUTION_X;
pub fn load_data(path: impl AsRef<Path>) -> Result<Array2<i16>> {
    println!("Using data from GEBCO Compilation Group (2019) GEBCO 2019 Grid (doi:10.5285/836f016a-33be-6ddc-e053-6c86abc0788e)");
    let file = netcdf::open(path)?;
    let var = &file
        .variable("elevation")
        .ok_or(ErrorKind::NonExistingVariable)?;

    let dim = var.dimensions();
    let r_dim = (
        dim[0].len() / RESOLUTION_X as usize,
        dim[1].len() / RESOLUTION_Y as usize,
    );
    let size = r_dim.0 * r_dim.1;

    let mut array = vec![0; size];
    assert!(size == var.values_strided_to(&mut array, None, None, &[RESOLUTION_X, RESOLUTION_Y])?);
    let data_array = Array2::from_shape_vec(r_dim, array).unwrap();

    let data_array = data_array.into_dimensionality::<Ix2>().unwrap();
    Ok(data_array)
}
