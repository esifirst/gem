use image::RgbImage;
use ndarray::ArrayView2;
use ndarray_stats::QuantileExt;
use palette::LinSrgb;

use error_chain::error_chain;

error_chain! {
    foreign_links {
        MinMaxError(ndarray_stats::errors::MinMaxError);
        IoError(std::io::Error);
    }
}

type Gradient = palette::gradient::Gradient<LinSrgb<f32>>;

use lazy_static::lazy_static;
lazy_static! {
    pub static ref FULL_GRADIENT: Gradient = Gradient::new(vec![
        LinSrgb::new(12u8, 7, 134).into_format(),
        LinSrgb::new(69u8, 3, 158).into_format(),
        LinSrgb::new(114u8, 0, 168).into_format(),
        LinSrgb::new(155u8, 23, 158).into_format(),
        LinSrgb::new(215u8, 86, 108).into_format(),
        LinSrgb::new(236u8, 120, 83).into_format(),
        LinSrgb::new(250u8, 157, 58).into_format(),
        LinSrgb::new(252u8, 199, 38).into_format(),
        LinSrgb::new(239u8, 248, 33).into_format(),
        LinSrgb::new(255u8, 0, 0).into_format(),
    ]);
}

pub trait Heatmap {
    fn heatmap(&self, gradient: &Gradient) -> Result<RgbImage>;
}

impl<'a, A> Heatmap for ArrayView2<'a, A>
where
    A: std::cmp::PartialOrd + Into<f32> + Copy,
{
    fn heatmap(&self, gradient: &Gradient) -> Result<RgbImage> {
        // TODO: Compute both in same pass
        let max: f32 = (*self.max()?).into();
        let min: f32 = (*self.min()?).into();

        let (height, width) = self.dim();

        let raw = self.iter().flat_map(|e| {
            let e: f32 = (*e).into();
            let color = gradient.get((e - min) / (max - min));
            let color = color.into_format::<u8>().into_components();
            vec![color.0, color.1, color.2]
        });

        let image = RgbImage::from_raw(width as u32, height as u32, raw.collect())
            .expect("container should have the right size for the image dimensions");

        Ok(image)
    }
}
