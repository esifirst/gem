extern crate nom;
use nom::{
    bytes::complete::take_while,
    bytes::streaming::take_till,
    combinator::{complete, map_res},
    sequence::tuple,
    IResult,
};

fn number(input: &str) -> IResult<&str, usize> {
    use std::str::FromStr;
    map_res(take_while(|e: char| e.is_digit(10)), usize::from_str)(input)
}

fn separator(input: &str) -> IResult<&str, &str> {
    take_till(|e: char| e.is_digit(10))(input)
}

pub fn pair(input: &str) -> IResult<&str, (usize, usize)> {
    let (input, (x, _, y)) = tuple((number, separator, complete(number)))(input)?;

    Ok((input, (x, y)))
}

use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::Result;

pub struct CheckPair {
    pub min: (usize, usize),
    pub max: (usize, usize),
}
impl rustyline::Helper for CheckPair {}
impl rustyline::highlight::Highlighter for CheckPair {}
impl rustyline::hint::Hinter for CheckPair {}
impl rustyline::completion::Completer for CheckPair {
    type Candidate = String;
}

impl Validator for CheckPair {
    fn validate_while_typing(&self) -> bool {
        true
    }
    fn validate(&self, ctx: &mut ValidationContext) -> Result<ValidationResult> {
        let input = ctx.input();

        Ok(match pair(input) {
            Ok((_, (a, b))) => ValidationResult::Invalid(Some(if a < self.min.0 {
                format!(
                    "\t First value is too small (got {}, expected at least {})",
                    a, self.min.0
                )
            } else if a > self.max.0 {
                format!(
                    "\t First value is too big (got {}, expected less than {})",
                    a, self.max.0
                )
            } else if b < self.min.1 {
                format!(
                    "\t Second value is too small (got {}, expected at least {})",
                    b, self.min.1
                )
            } else if b > self.max.1 {
                format!(
                    "\t Second value is too big (got {}, expected less than {})",
                    b, self.max.1
                )
            } else {
                return Ok(ValidationResult::Valid(None));
            })),
            Err(nom::Err::Incomplete(_)) => ValidationResult::Incomplete,
            Err(e) => ValidationResult::Invalid(Some(format!("\t {}", e))),
        })
    }
}
