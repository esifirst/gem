use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer};
use vulkano::command_buffer::{AutoCommandBufferBuilder, CommandBuffer};
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::device::{Device, DeviceExtensions};
use vulkano::image::{Dimensions, ImageUsage, ImageLayout, MipmapsCount, StorageImage};
use vulkano::image::immutable::ImmutableImage;
// use vulkano::image::attachment::AttachmentImage;
use vulkano::instance::{Instance, InstanceExtensions, PhysicalDevice};
use vulkano::pipeline::ComputePipeline;
// use vulkano::sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode};
use vulkano::sync::GpuFuture;
// use vulkano::sync;

use std::sync::Arc;

use ndarray::ArrayView2;

use error_chain::error_chain;

error_chain!{
    foreign_links {
        InstanceCreationError(vulkano::instance::InstanceCreationError);

        FlushError(vulkano::sync::FlushError);
        WriteLockError(vulkano::buffer::cpu_access::WriteLockError);

        CommandBufferBuildError(vulkano::command_buffer::BuildError);
        CommandBufferExecError(vulkano::command_buffer::CommandBufferExecError);
        CopyBufferImageError(vulkano::command_buffer::CopyBufferImageError);

        PersistentDescriptorSetBuildError(vulkano::descriptor::descriptor_set::PersistentDescriptorSetBuildError);
        PersistentDescriptorSetError(vulkano::descriptor::descriptor_set::PersistentDescriptorSetError);
        ImageCreationError(vulkano::image::sys::ImageCreationError);
        DeviceMemoryAllocError(vulkano::memory::DeviceMemoryAllocError);

        ComputePipelineCreationError( vulkano::pipeline::ComputePipelineCreationError);
        DeviceCreationError(vulkano::device::DeviceCreationError);

        DispatchError(vulkano::command_buffer::DispatchError);

        OomError(vulkano::OomError);
    }

    errors {
        NoQueuesCreated {
            description("Unable to find the created queue"),
            display("Unable to find the created queue"),
        }
        NoQueuesCompatible {
            description("Unable to find a queue that match request"),
            display("Unable to find a queue that is Compute but not Graphical"),
        }
        NoPhysicalDevice {
            description("Unable to find a physical device"),
            display("Unable to find a physical device"),
        }
    }
}

use vulkano::command_buffer::AutoCommandBuffer;
use vulkano::device::Queue;
pub struct Vulkan {
    command_buffer: Arc<AutoCommandBuffer>,
    queue: Arc<Queue>,
    kernel_buffer: Arc<CpuAccessibleBuffer<[i16]>>,
    png_buf: Arc<CpuAccessibleBuffer<[i16]>>,
}

impl Vulkan {
    pub fn init(data_array: &ArrayView2<i16>) -> Result<Vulkan> {
        let extensions = InstanceExtensions {
            ext_debug_utils: true,
            ..InstanceExtensions::none()
        };

        let layers = vec![
            "VK_LAYER_LUNARG_standard_validation",
            //        "VK_LAYER_LUNARG_api_dump",
        ];

        let instance = Instance::new(None, &extensions, layers)?;
        let physical = PhysicalDevice::enumerate(&instance).next().ok_or(ErrorKind::NoPhysicalDevice)?;

        println!("Limits mem: {:?}", physical.limits().max_compute_shared_memory_size());
        println!("Limits 2D img: {:?}", physical.limits().max_image_dimension_2d());
        println!("Limits uniform buffer: {:?}", physical.limits().max_uniform_buffer_range());
        println!("Limits SSBO: {:?}", physical.limits().max_storage_buffer_range());
        println!("Limits WG nb: {:?}", physical.limits().max_compute_work_group_count());
        println!("Limits WG inv: {:?}", physical.limits().max_compute_work_group_invocations());
        println!("Limits WG size: {:?}", physical.limits().max_compute_work_group_size());

        let queue_family = physical.queue_families()
            .find(|&q| q.supports_compute() && !q.supports_graphics())
            .ok_or(ErrorKind::NoQueuesCompatible)?;

        let (device, mut queues) = Device::new(physical, physical.supported_features(),
        &DeviceExtensions{khr_storage_buffer_storage_class:true, ..DeviceExtensions::none()},
        [(queue_family, 0.5)].iter().cloned())?;
        let queue = queues.next().ok_or(ErrorKind::NoQueuesCreated)?;

        println!("Device extensions: {:?}", device.loaded_extensions());
        println!("Device initialized with queue {}", queue_family.id());

        let pipeline = Arc::new({
            mod cs {
                vulkano_shaders::shader!{
                    ty: "compute",
                    src: "
#version 450

                    precision highp int;

                layout(local_size_x = 32, local_size_y = 32) in;

                layout(set = 0, binding = 0, r16i) uniform  readonly iimage2D src;
                layout(set = 0, binding = 1, r16i) uniform  readonly iimage2D ker;
                layout(set = 0, binding = 2, r16i) uniform writeonly iimage2D res;

                void main() {
                    int delta = 0;
                    for (int i=-50; i<=50; ++i) {
                        for(int j=-50; j<=50; ++j) {
                            int ref = imageLoad(src, ivec2(gl_GlobalInvocationID.x + i, gl_GlobalInvocationID.y + j)).x;
                            int got = imageLoad(ker, ivec2(50 + i, 50 + j)).x;

                            delta += abs(ref - got);
                        }
                    }

        /*            float conv = 0;
                    for (int m=0; m<=11; ++m) {
                        for(int n=0; n<=11; ++n) {
                            int ref = imageLoad(src, ivec2(m - gl_GlobalInvocationID.x, n - gl_GlobalInvocationID.y)).x;
                            int got = imageLoad(ker, ivec2(m, n)).x;

                            conv += ref * got;
                        }
                    }*/

                    if(delta == 0) {
                        imageStore(res, ivec2(0,0), ivec4(gl_GlobalInvocationID.x));
                        imageStore(res, ivec2(1,0), ivec4(gl_GlobalInvocationID.y));
                        // imageStore(res, ivec2(2,0), ivec4(conv));
                    }
                }
"
                }
            }
            let shader = cs::Shader::load(device.clone())?;
            ComputePipeline::new(device.clone(), &shader.main_entry_point(), &())?
        });

        let size = Dimensions::Dim2d {
            width: data_array.dim().1 as u32,
            height: data_array.dim().0 as u32,
        };

        let (src_img, future) = {
            let source = CpuAccessibleBuffer::from_iter(device.clone(),
            BufferUsage::transfer_source(),
            data_array.iter().cloned())?;
            let usage = ImageUsage {
                transfer_destination: true,
                storage: true,
                input_attachment: true,
                ..ImageUsage::none()
            };

            let (buffer, init) =
                ImmutableImage::uninitialized(device.clone(),
                size,
                vulkano::format::R16Sint,
                MipmapsCount::One,
                usage,
                ImageLayout::General,
                device.active_queue_families())?;

            let cb = AutoCommandBufferBuilder::new(device.clone(), queue.family())?
                .copy_buffer_to_image_dimensions(source,
                                                 init,
                                                 [0, 0, 0],
                                                 size.width_height_depth(),
                                                 0,
                                                 size.array_layers_with_cube(),
                                                 0)?
                .build()?;

            let future = match cb.execute(queue.clone()) {
                Ok(f) => f,
                Err(_) => unreachable!(),
            };

            (buffer, future)
        };

        let ker_size = Dimensions::Dim2d{ height: 11, width: 11 };
        let kernel_buffer = CpuAccessibleBuffer::from_iter(device.clone(),
            BufferUsage::transfer_source(),
            vec![0i16; 11 * 11].into_iter()
        )?;
        let kernel_img = {
            let usage = ImageUsage {
                transfer_destination: true,
                storage: true,
                input_attachment: true,
                ..ImageUsage::none()
            };

            StorageImage::with_usage(
                device.clone(),
                ker_size,
                vulkano::format::R16Sint,
                usage,
                Some(queue.family()),
                )?
        };

        let out_size = Dimensions::Dim2d{ height: 1, width: 3 };
        let out_img = StorageImage::new(device.clone(), out_size,
        vulkano::format::R16Sint, Some(queue.family()))?;

        future.then_signal_fence_and_flush()?.wait(None)?;

        let set = Arc::new(PersistentDescriptorSet::start(pipeline.clone(), 0)
                           .add_image(src_img.clone())?
                           .add_image(kernel_img.clone())?
                           .add_image(out_img.clone())?
                           .build()?
                          );

        let png_buf = CpuAccessibleBuffer::from_iter(device.clone(), BufferUsage::all(),
        (0 .. size.num_texels()).map(|_| 0i16))?;


        let command_buffer = AutoCommandBufferBuilder::primary(device.clone(), queue.family())?
            .copy_buffer_to_image_dimensions(kernel_buffer.clone(),
                                             kernel_img.clone(),
                                             [0, 0, 0],
                                             ker_size.width_height_depth(),
                                             0,
                                             ker_size.array_layers_with_cube(),
                                             0)?
            .dispatch([
                      data_array.dim().1 as u32 / 32 + 1,
                      data_array.dim().0 as u32 / 32 + 1,
                      1],
                      pipeline.clone(),
                      set.clone(),
                      ()
                     )?
            .copy_image_to_buffer(out_img.clone(), png_buf.clone())?
            .build()?;

        Ok(Vulkan {
            queue,
            command_buffer: command_buffer,
            png_buf,
            kernel_buffer,
        })
    }

    pub fn do_search(&mut self, kernel: &ArrayView2<i16>) -> Result<(i16, i16)> {
        {
            let mut mapping = self.kernel_buffer.write()?;
            kernel.iter().zip(mapping.iter_mut()).for_each(|(i, o)| *o = *i);
        }

        self.command_buffer.execute(self.queue.clone())?
            .then_signal_fence_and_flush()?
            .wait(None)?;

        /* Export to PNG */ {
            let buffer_content = self.png_buf.read().expect("Read back the buffer");
            let res = ndarray::ArrayView::from_shape([3], &buffer_content)
                .expect("Build array from shape");

            Ok((res[0], res[1]))
        }
    }
}

