mod data;
use data::load_data;

mod heatmap;

// mod vulkan;
mod vk2;

// use vulkan::Vulkan;
use vk2::Vulkan;

use ndarray::s;
use rand::{thread_rng, Rng};

use rustyline::error::ReadlineError;
use rustyline::Editor;

struct Time(std::time::Instant, &'static str);

impl Time {
    fn start(name: &'static str) -> Self {
        Time(std::time::Instant::now(), name)
    }
}

impl Drop for Time {
    fn drop(&mut self) {
        println!("\t[TIMING] {} took {:?}", self.1, self.0.elapsed());
    }
}

mod pair;
use pair::{pair, CheckPair};

fn main() {
    let (data_array, mut vulkan) = {
        let _timer = Time::start("Init");
        let data_array = load_data("GEBCO_2019.nc").unwrap();
        let v = Vulkan::init(&data_array.view()).expect("Initialisation");

        (data_array, v)
    };

    println!("Data {:?}", data_array);

    let mut rl = Editor::new();
    rl.set_helper(Some(CheckPair {
        min: (10, 10),
        max: (data_array.dim().0 - 11, data_array.dim().1 - 11),
    }));
    let mut ok = 0;
    let mut ko = 0;
    loop {
        let readline = rl.readline(">> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());

                let (x, y) = pair(&line).unwrap().1;

                let mut rng = thread_rng();
                let offset = rng.gen_range(-20, 20);

                let kernel = data_array
                    .slice(s!((x - 10)..=(x + 10), (y - 10)..=(y + 10)))
                    .mapv(|e| offset + e + rng.gen_range(-10, 10));

                let _timer = Time::start("Computation");
                let res = vulkan.do_search(&kernel.view()).expect("DoSearch");

                if res == (x, y) {
                    println!("Got result {:?}", res);
                    ok += 1;
                } else {
                    ko += 1;
                    println!("Returned {:?}, but expecting {:?}", res, (x, y));
                }
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }

    println!("Result {} ok, {} ko", ok, ko);
}
